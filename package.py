name = "jfLogging"

version = "0.3.0"

authors = [
    "n.kelkar"
]

description = \
    """
    Python-based JFP logging module.
    """

requires = [
    "python",
    "PyYAML"
]

build_command = 'python {root}/rezbuild.py {install}'


def commands():
    env.PYTHONPATH.append("{root}/python")


tests = {
    'unit': {
        'command': 'python -m unittest discover',
        'requires': ['python'],
    }
}