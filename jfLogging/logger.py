import os
import logging
import logging.config
import sys
from jfLogging import parser

"""
This function is used to setup root logging configuration,
* If JF_LOG_CFG variable is set, then use it for logging configuration path
* Since we are using yaml configuration, we need yaml module to load configuration file
* Set Root logger configuration using `logging.config.dictConfig()`
* Any exception results in setting up root logger in default configuration. 
"""

pwd = os.path.abspath(os.path.dirname(__file__))
DEFAULT_LOG_PATH = os.path.join(pwd, 'config', 'default_config.yml')
DEFAULT_LOG_LEVEL = logging.INFO


# Function to configure root logger
def setup_logging(default_path=DEFAULT_LOG_PATH, default_level=DEFAULT_LOG_LEVEL, env_key='JF_LOG_CFG'):
    """
    Logging Setup
    Args:
        default_path (Str): Logging configuration path
        default_level (Str): Default logging level
        env_key (Str): Logging config path set in environment variable

    """
    path = os.getenv(env_key, default_path)
    try:
        config = parser.parse_config(path)
        logging.config.dictConfig(config)
    except Exception as e:
        print('Error in Logging Configuration. Using default configs', e)
        logging.basicConfig(level=default_level, stream=sys.stdout)
