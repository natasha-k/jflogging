import logging


class InfoFilter(logging.Filter):
    def filter(self, record):
        """
        Return Log Record Object based on condition - Return only info logs
        Args:
            record: Log Record Object

        Returns: Log Record Object

        """
        assert isinstance(record, logging.LogRecord)
        if record.levelno == logging.INFO:
            return record
