import os
import unittest
import logging

from jfLogging import logger


class MockLogHandler(logging.Handler):
    """ Mock handler for testing log messages"""
    records = []

    def emit(self, record):
        self.records.append(record.getMessage())


class BaseTest(unittest.TestCase):
    """ Base Class for logging tests """
    def setUp(self):
        pwd = os.path.abspath(os.path.dirname(__file__))
        log_path = os.path.join(pwd, 'resources', 'test_logger_config.yml')
        logger.setup_logging(default_path=log_path)

    def tearDown(self):
        logging.shutdown()
        log_file = 'test_info.log'
        if os.path.exists(log_file):
            os.remove(log_file)

    def test_root_logger(self):
        root_logger = logging.getLogger()
        assert root_logger.level == logging.NOTSET
        assert len(root_logger.handlers) == 2

    def test_jf_default_logger(self):
        log = logging.getLogger('jf_default')
        assert log.level == logging.DEBUG
        assert len(log.handlers) == 1

    def test_logging(self):
        log = logging.getLogger('jf_test')
        log.info('Info Test')
        log.debug('Debug Test')
        handler = log.handlers[0]
        self.assertEqual(len(handler.records), 1)
        self.assertEqual(handler.records[0], "Info Test")


if __name__ == '__main__':
    unittest.main()
