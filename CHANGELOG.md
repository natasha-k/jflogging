# Change Log

All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.1] - 2021-05-19
 
### Added

- Added a new changelog

## [0.2.0] - 2021-05-19
 
### Added

- Added a new changelog

## [0.1.0] - 2021-05-19
 
### Added

- rez packaging files
   
### Changed
 
### Fixed

- Fixed some of the jlibs imports